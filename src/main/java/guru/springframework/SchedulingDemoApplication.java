package guru.springframework;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@EnableScheduling
@SpringBootApplication
public class SchedulingDemoApplication {

    @Scheduled(cron = "0 0 8 * * *")
    public static void notifyUser() {
        try {
            OkHttpClient client = new OkHttpClient();

            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, "phone=6285156973768&message=GoodMorning");
            Request request = new Request.Builder()
                    .url("https://console.wablas.com/api/send-message")
                    .post(body)
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .addHeader("Authorization", "pLsLhfJP3mKFkFp0OguPaAIJOFMA2doZtww3NkpKC04gH9X053respKNPpaGGUMn")
                    .build();

            Response response = client.newCall(request).execute();
            System.out.println("iniResponse" + response.toString());
            System.out.println("Current time is :: " + Calendar.getInstance().getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean hasNotified(List<String> list, String id) {
        for (String s : list) {
            if (id.equals(s)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        SpringApplication.run(SchedulingDemoApplication.class, args);
        notifyUser();
    }

}
